// -*- C++ -*-
//
// Package:    GenParticleAnalysis/GenParticleAnalyzer
// Class:      GenParticleAnalyzer
// 
/**\class GenParticleAnalyzer GenParticleAnalyzer.cc GenParticleAnalysis/GenParticleAnalyzer/plugins/GenParticleAnalyzer.cc

 Description: [one line class summary]

 Implementation:
     [Notes on implementation]
*/
//
// Original Author:  Young Ho Shin
//         Created:  Thu, 04 Jun 2015 20:12:40 GMT
//
//


// system include files
#include <memory>
#include <vector>
#include <iostream>

// user include files
#include "FWCore/Framework/interface/Frameworkfwd.h"
#include "FWCore/Framework/interface/EDAnalyzer.h"

#include "FWCore/Framework/interface/Event.h"
#include "FWCore/Framework/interface/MakerMacros.h"

#include "FWCore/ParameterSet/interface/ParameterSet.h"

#include "DataFormats/HepMCCandidate/interface/GenParticle.h"
#include "DataFormats/HepMCCandidate/interface/GenParticleFwd.h"


#include "TFile.h"
#include "TTree.h"

//
// class declaration
//

class GenParticleAnalyzer : public edm::EDAnalyzer {
   public:
      explicit GenParticleAnalyzer(const edm::ParameterSet&);
      ~GenParticleAnalyzer();

      static void fillDescriptions(edm::ConfigurationDescriptions& descriptions);


   private:
      virtual void beginJob() override;
      virtual void analyze(const edm::Event&, const edm::EventSetup&) override;
      virtual void endJob() override;

      //virtual void beginRun(edm::Run const&, edm::EventSetup const&) override;
      //virtual void endRun(edm::Run const&, edm::EventSetup const&) override;
      //virtual void beginLuminosityBlock(edm::LuminosityBlock const&, edm::EventSetup const&) override;
      //virtual void endLuminosityBlock(edm::LuminosityBlock const&, edm::EventSetup const&) override;

      // ----------member data ---------------------------

			// inputs
			edm::EDGetTokenT<reco::GenParticleCollection> genParticleCollectionToken_;

			// outputs
			TFile* ofile_;
			TTree* tree_;
			std::vector<float> genParticlesPt_;
			std::vector<float> genParticlesEta_;
			std::vector<float> genParticlesPhi_;
			std::vector<float> genParticlesMass_;
};

//
// constants, enums and typedefs
//

//
// static data member definitions
//

//
// constructors and destructor
//
GenParticleAnalyzer::GenParticleAnalyzer(const edm::ParameterSet& iConfig)
{
   //now do what ever initialization is needed
	 ofile_ = new TFile("file.root","RECREATE","GenParticleAnalyzer output file");
	 tree_ = new TTree("tree", "GenParticleAnalyzer output tree");
	 tree_->Branch("pt"   , &genParticlesPt_   ) ;
	 tree_->Branch("eta"  , &genParticlesEta_  ) ;
	 tree_->Branch("phi"  , &genParticlesPhi_  ) ;
	 tree_->Branch("mass" , &genParticlesMass_ ) ;

}


GenParticleAnalyzer::~GenParticleAnalyzer()
{
 
   // do anything here that needs to be done at desctruction time
   // (e.g. close files, deallocate resources etc.)
	 ofile_->Write();
	 delete ofile_;

}


//
// member functions
//

// ------------ method called for each event  ------------
void
GenParticleAnalyzer::analyze(const edm::Event& iEvent, const edm::EventSetup& iSetup)
{
   using namespace edm;

	 Handle<reco::GenParticleCollection> genParticles;
	 iEvent.getByLabel("genParticles", genParticles);

	 genParticlesPt_   . clear();
	 genParticlesEta_  . clear();
	 genParticlesPhi_  . clear();
	 genParticlesMass_ . clear();

	 int index = 0;
	 for (auto it = genParticles->begin(); it != genParticles->end(); it++) {
		 index ++;
		 // std::cout << "Running over particle " << index << std::endl;
		 auto candidate = *it;
		 if ( candidate.status()!=1 ) continue;
		 if ( candidate.pt()<2 ) continue;
		 genParticlesPt_   . push_back( candidate . pt()   );
		 genParticlesEta_  . push_back( candidate . eta()  );
		 genParticlesPhi_  . push_back( candidate . phi()  );
		 genParticlesMass_ . push_back( candidate . mass() );
	 }
	 tree_->Fill();


#ifdef THIS_IS_AN_EVENT_EXAMPLE
   Handle<ExampleData> pIn;
   iEvent.getByLabel("example",pIn);
#endif
   
#ifdef THIS_IS_AN_EVENTSETUP_EXAMPLE
   ESHandle<SetupData> pSetup;
   iSetup.get<SetupRecord>().get(pSetup);
#endif
}


// ------------ method called once each job just before starting event loop  ------------
void 
GenParticleAnalyzer::beginJob()
{
}

// ------------ method called once each job just after ending the event loop  ------------
void 
GenParticleAnalyzer::endJob() 
{
}

// ------------ method called when starting to processes a run  ------------
/*
void 
GenParticleAnalyzer::beginRun(edm::Run const&, edm::EventSetup const&)
{
}
*/

// ------------ method called when ending the processing of a run  ------------
/*
void 
GenParticleAnalyzer::endRun(edm::Run const&, edm::EventSetup const&)
{
}
*/

// ------------ method called when starting to processes a luminosity block  ------------
/*
void 
GenParticleAnalyzer::beginLuminosityBlock(edm::LuminosityBlock const&, edm::EventSetup const&)
{
}
*/

// ------------ method called when ending the processing of a luminosity block  ------------
/*
void 
GenParticleAnalyzer::endLuminosityBlock(edm::LuminosityBlock const&, edm::EventSetup const&)
{
}
*/

// ------------ method fills 'descriptions' with the allowed parameters for the module  ------------
void
GenParticleAnalyzer::fillDescriptions(edm::ConfigurationDescriptions& descriptions) {
  //The following says we do not know what parameters are allowed so do no validation
  // Please change this to state exactly what you do use, even if it is no parameters
  edm::ParameterSetDescription desc;
  desc.setUnknown();
  descriptions.addDefault(desc);
}

//define this as a plug-in
DEFINE_FWK_MODULE(GenParticleAnalyzer);
