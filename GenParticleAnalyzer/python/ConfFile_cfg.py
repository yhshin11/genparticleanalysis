import FWCore.ParameterSet.Config as cms

process = cms.Process("Demo")

process.load("FWCore.MessageService.MessageLogger_cfi")

process.maxEvents = cms.untracked.PSet( input = cms.untracked.int32(2000) )

process.source = cms.Source("PoolSource",
    # replace 'myfile.root' with the source file you want to use
    fileNames = cms.untracked.vstring(
			'root://xrootd-cms.infn.it//store/mc/Phys14DR/QCD_Pt-1000to1400_Tune4C_13TeV_pythia8/AODSIM/PU20bx25_trkalmb_castor_PHYS14_25_V1-v1/00000/144E0B9E-716F-E411-A298-0025905964B2.root'
        # 'file:myfile.root'
    )
)

process.demo = cms.EDAnalyzer('GenParticleAnalyzer'
)


process.p = cms.Path(process.demo)
